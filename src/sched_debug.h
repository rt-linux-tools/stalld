/* SPDX-License-Identifier: GPL-2.0-or-later */
#define OLD_TASK_FORMAT  1
#define NEW_TASK_FORMAT  2
#define TASK_MARKER	"runnable tasks:"

extern struct stalld_backend sched_debug_backend;
